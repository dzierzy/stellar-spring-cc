package lab.stellar.web;

import lab.stellar.entities.Planet;
import lab.stellar.entities.PlanetarySystem;
import lab.stellar.service.StellarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class SystemsController {

    @Autowired
    private StellarService service;

    //@RequestMapping(path = "/systems", method = RequestMethod.GET)
    @GetMapping(path="/systems")
    public String getAllSystems(
            Model model,
            @RequestParam(value="phrase", required = false) String phrase){
        List<PlanetarySystem> systems = phrase==null ?
                service.getSystems() : service.getSystemsByName(phrase);
        model.addAttribute("systems", systems);
        return "systems";
    }

    @GetMapping("/planets")
    public String getPlanetsFromSystem(Model model, @RequestParam("systemId") int id){

        List<Planet> planets = service.getPlanets(service.getSystemById(id));
        model.addAttribute("planets", planets);
        return "planets";

    }

}
