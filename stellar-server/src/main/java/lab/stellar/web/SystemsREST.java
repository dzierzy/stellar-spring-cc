package lab.stellar.web;

import lab.stellar.entities.Planet;
import lab.stellar.entities.PlanetarySystem;
import lab.stellar.service.StellarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/webapi")
@CrossOrigin(origins = "*")
public class SystemsREST {

    @Autowired
    private StellarService service;

    @GetMapping("/systems")
    public List<PlanetarySystem> getSystems(){
        return service.getSystems();
    }

    @GetMapping("/systems/{id}")
    public ResponseEntity<PlanetarySystem> getSystem(@PathVariable("id") int id){
        try{
            PlanetarySystem ps =  service.getSystemById(id);
            return new ResponseEntity<>(ps, HttpStatus.OK);
        } catch(Exception e){
            // FIXME fix underlying bug instead
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/systems/{id}/planets")
    public List<Planet> getPlanetsBySystem(@PathVariable("id") int systemId){
        return service.getPlanets(service.getSystemById(systemId));
    }

    @PostMapping("/systems")
    public PlanetarySystem addSystem(@RequestBody PlanetarySystem system){
        return service.addPlanetarySystem(system);
    }

}
